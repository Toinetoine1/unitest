package fr.toinetoine1.unitest.utils;

import java.lang.reflect.Field;

/**
 * Created by Toinetoine1 on 21/04/2020.
 */

public class Reflections {

    public static Field getField(Class<?> clazz, String fieldName){
        Field field = null;
        try {
            field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return field;
    }

}
