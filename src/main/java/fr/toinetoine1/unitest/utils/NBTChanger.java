package fr.toinetoine1.unitest.utils;

import net.minecraft.server.v1_10_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Toinetoine1 on 21/04/2020.
 */

public class NBTChanger {

    public static ItemStack addNBTTag(ItemStack itemStack, String key, String value){
        net.minecraft.server.v1_10_R1.ItemStack stack = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound nbtTagCompound = stack.hasTag() ? stack.getTag() : new NBTTagCompound();

        nbtTagCompound.setString(key, value);
        stack.setTag(nbtTagCompound);

        return CraftItemStack.asBukkitCopy(stack);
    }

    public static boolean hasNBTTag(ItemStack itemStack, String key){
        net.minecraft.server.v1_10_R1.ItemStack stack = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound nbtTagCompound = stack.getTag();
        if(nbtTagCompound == null)
            return false;

        return nbtTagCompound.getString(key) != null;
    }

    public static String getNBTTag(ItemStack itemStack, String key){
        net.minecraft.server.v1_10_R1.ItemStack stack = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound nbtTagCompound = stack.getTag();
        if(nbtTagCompound == null)
            return null;

        return nbtTagCompound.getString(key);
    }

}
