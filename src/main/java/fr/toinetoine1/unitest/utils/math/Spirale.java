package fr.toinetoine1.unitest.utils.math;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

/**
 * Created by Toinetoine1 on 21/04/2020.
 */

public class Spirale {

    public static void drawSpirale(Player player) {
        Location location = player.getLocation();
        float radius = 0.1f;

        for (double i = 0; i < 2 * Math.PI; i += 2 * Math.PI / 400) {
            double x = Math.cos(i) * radius;
            double z = Math.sin(i) * radius;

            location.add(x, 0, z);
            player.spawnParticle(Particle.REDSTONE, location, 1);
            location.subtract(x, 0, z);

            radius += .03f;
        }
    }

}
