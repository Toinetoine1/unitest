package fr.toinetoine1.unitest.utils;

import net.minecraft.server.v1_10_R1.Entity;
import net.minecraft.server.v1_10_R1.EntityTypes;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * Created by Toinetoine1 on 21/04/2020.
 */

public class EntityRegister {

    public static void registerEntity(String name, int id, Class<? extends Entity> clazz) {
        try {
            Class<?> entityTypeClass = EntityTypes.class;

            Field entityType_d = Reflections.getField(entityTypeClass, "d");
            Field entityType_f = Reflections.getField(entityTypeClass, "f");

            Map<Class, String> d = (Map) entityType_d.get(entityType_d);
            Map<Class, Integer> f = (Map) entityType_f.get(entityType_f);

            d.put(clazz, name);
            f.put(clazz, id);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    /*
        public static void registerEntity(String name, int id, Class<? extends Entity> clazz) {
        try {
            Class<?> entityTypeClass = EntityTypes.class;

            Field[] fields = new Field[]{
                    entityTypeClass.getDeclaredField("c"),
                    entityTypeClass.getDeclaredField("d"),
                    entityTypeClass.getDeclaredField("e"),
                    entityTypeClass.getDeclaredField("f"),
                    entityTypeClass.getDeclaredField("g")
            };
            Map[] maps = new Map[fields.length];

            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];

                field.setAccessible(true);
                maps[i] = (Map) field.get(null);
            }

            maps[0].put(name, clazz);
            maps[1].put(clazz, name);

            maps[2].put(id, clazz);
            maps[3].put(clazz, id);

            maps[4].put(name, id);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
     */

    /*
        private static void a(Class<? extends Entity> var0, String var1, int var2) {
        if (c.containsKey(var1)) {
            throw new IllegalArgumentException("ID is already registered: " + var1);
        } else if (e.containsKey(var2)) {
            throw new IllegalArgumentException("ID is already registered: " + var2);
        } else if (var2 == 0) {
            throw new IllegalArgumentException("Cannot register to reserved id: " + var2);
        } else if (var0 == null) {
            throw new IllegalArgumentException("Cannot register null clazz for id: " + var2);
        } else {
            c.put(var1, var0);
            d.put(var0, var1);
            e.put(var2, var0);
            f.put(var0, var2);
            g.put(var1, var2);
        }
    }
     */

}
