package fr.toinetoine1.unitest;

import fr.toinetoine1.unitest.commands.MountCommand;
import fr.toinetoine1.unitest.commands.SpheroCommand;
import fr.toinetoine1.unitest.listeners.*;
import fr.toinetoine1.unitest.entity.CustomEnderman;
import fr.toinetoine1.unitest.utils.EntityRegister;
import fr.toinetoine1.unitest.utils.NBTChanger;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

/**
 * Created by Toinetoine1 on 21/04/2020.
 */

public class UniTest extends JavaPlugin {

    private Map<UUID, Inventory> inventories;
    private List<Entity> customEntity;

    @Override
    public void onEnable() {
        this.inventories = new HashMap<>();
        this.customEntity = new ArrayList<>();

        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerInteractListener(this), this);
        getServer().getPluginManager().registerEvents(new BlockBreakListener(this), this);
        getServer().getPluginManager().registerEvents(new InventoryCloseListener(this), this);
        getServer().getPluginManager().registerEvents(new InventoryClickListener(this), this);

        EntityRegister.registerEntity("custom_enderman", 58, CustomEnderman.class);

        getCommand("mount").setExecutor(new MountCommand(this));
        getCommand("sphero").setExecutor(new SpheroCommand(this));

        registerCraft();
    }

    private void registerCraft() {
        ItemStack result = new ItemStack(Material.SNOW_BALL);
        result = NBTChanger.addNBTTag(result, "snowball", "null");

        ShapedRecipe recipe = new ShapedRecipe(result);
        recipe.shape("WW ", "WS ", " S ");
        recipe.shape(" WW", " SW", " S ");

        recipe.setIngredient('W', Material.WOOD);
        recipe.setIngredient('S', Material.STICK);

        getServer().addRecipe(recipe);
    }

    @Override
    public void onDisable() {
        customEntity.forEach(Entity::remove);
    }

    public List<Entity> getCustomEntity() {
        return customEntity;
    }

    public Map<UUID, Inventory> getInventories() {
        return inventories;
    }
}
