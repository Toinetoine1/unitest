package fr.toinetoine1.unitest.entity;

import net.minecraft.server.v1_10_R1.*;
import org.bukkit.Location;

import javax.annotation.Nullable;
import java.lang.reflect.Field;

/**
 * Created by Toinetoine1 on 21/04/2020.
 */

public class CustomEnderman extends EntityEnderman {

    private boolean bool;

    public CustomEnderman(World world, Location location) {
        super(world);

        this.setPosition(location.getX(), location.getY(), location.getZ());
        world.addEntity(this);

        bool = false;
    }

    @Override
    public void g(float f, float f1) {
        if (this.isVehicle() && this.cP()) {
            EntityLiving entityliving = (EntityLiving) this.bw();
            this.yaw = entityliving.yaw;
            this.lastYaw = this.yaw;
            this.pitch = entityliving.pitch * 0.5F;
            this.setYawPitch(this.yaw, this.pitch);
            this.aO = this.yaw;
            this.aQ = this.aO;
            f = entityliving.bf * 0.5F;
            f1 = entityliving.bg;
            if (f1 <= 0.0F) {
                f1 *= 0.25F;
            }

            Field jump = null;
            try {
                jump = EntityLiving.class.getDeclaredField("be");
            } catch (NoSuchFieldException ex) {
                ex.printStackTrace();
            }
            jump.setAccessible(true);

            try {
                if (jump.getBoolean(entityliving) && !bool && this.onGround) {
                    this.motY = 0.6;
                    if (this.hasEffect(MobEffects.JUMP)) {
                        this.motY += (float) (this.getEffect(MobEffects.JUMP).getAmplifier() + 1) * 0.1F;
                    }

                    bool = true;
                    this.impulse = true;
                    if (f1 > 0.0F) {
                        float f2 = MathHelper.sin(this.yaw * 0.017453292F);
                        float f3 = MathHelper.cos(this.yaw * 0.017453292F);
                        this.motX += -0.4F * f2 * 1f;
                        this.motZ += 0.4F * f3 * 1f;
                    }

                }
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }

            this.aS = this.cp() * 0.1F;


            this.l((float) this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).getValue());
            super.g(f, f1);


            if (this.onGround) {
                bool = false;
            }

            this.aG = this.aH;
            double d0 = this.locX - this.lastX;
            double d1 = this.locZ - this.lastZ;
            float f4 = MathHelper.sqrt(d0 * d0 + d1 * d1) * 4.0F;
            if (f4 > 1.0F) {
                f4 = 1.0F;
            }

            this.aH += (f4 - this.aH) * 0.4F;
            this.aI += this.aH;
        } else {
            this.aS = 0.02F;
            super.g(f, f1);
        }

    }

    public boolean cP() {
        Entity entity = this.bw();
        return entity instanceof EntityLiving;
    }

    @Nullable
    public Entity bw() {
        return this.bx().isEmpty() ? null : this.bx().get(0);
    }

    public boolean bA() {
        Entity entity = this.bw();
        return entity instanceof EntityHuman ? ((EntityHuman) entity).cO() : !this.world.isClientSide;
    }

}
