package fr.toinetoine1.unitest.listeners;

import fr.toinetoine1.unitest.UniTest;
import fr.toinetoine1.unitest.utils.NBTChanger;
import fr.toinetoine1.unitest.utils.math.Spirale;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by Toinetoine1 on 21/04/2020.
 */

public class PlayerInteractListener implements Listener {

    private UniTest uniTest;

    public PlayerInteractListener(UniTest uniTest) {
        this.uniTest = uniTest;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();

        if(action.equals(Action.LEFT_CLICK_BLOCK) || action.equals(Action.LEFT_CLICK_AIR)){
            Entity vehicle = player.getVehicle();
            if(vehicle != null && vehicle.getType() == EntityType.ENDERMAN){
                Enderman enderman = (Enderman) vehicle;
                Location location = enderman.getLocation().clone().subtract(0, 1, 0);

                enderman.setCarriedMaterial(location.getBlock().getState().getData());
            }
        } else {
            ItemStack item = event.getItem();
            if (item != null) {
                if (item.getType() == Material.SNOW_BALL && NBTChanger.hasNBTTag(item, "snowball")) {
                    Spirale.drawSpirale(player);
                } else if(item.getType() == Material.GLASS_BOTTLE && NBTChanger.hasNBTTag(item, "uuid")){
                    player.openInventory(uniTest.getInventories().get(UUID.fromString(NBTChanger.getNBTTag(item, "uuid"))));
                }
            }
        }
    }

}
