package fr.toinetoine1.unitest.listeners;

import fr.toinetoine1.unitest.UniTest;
import fr.toinetoine1.unitest.utils.NBTChanger;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;

import java.util.UUID;

/**
 * Created by Toinetoine1 on 22/04/2020.
 */

public class BlockBreakListener implements Listener {

    private UniTest uniTest;

    public BlockBreakListener(UniTest uniTest) {
        this.uniTest = uniTest;
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();

        if (block.getType() == Material.LOG) {
            ItemStack itemStack = player.getInventory().getItemInMainHand();

            if (itemStack.getType() == Material.WOOL) {
                Wool wool = (Wool) itemStack.getData();

                if (wool.getColor().equals(DyeColor.ORANGE)) {
                    event.setCancelled(true);
                    block.setType(Material.AIR);
                    ItemStack glassBottle = new ItemStack(Material.GLASS_BOTTLE);
                    UUID uuid = UUID.randomUUID();

                    glassBottle = NBTChanger.addNBTTag(glassBottle, "uuid", uuid.toString());
                    uniTest.getInventories().put(uuid, uniTest.getServer().createInventory(null, 9, uuid.toString()));

                    player.getWorld().dropItemNaturally(block.getLocation(), glassBottle);
                }
            }

        }
    }

}
