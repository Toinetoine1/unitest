package fr.toinetoine1.unitest.listeners;

import fr.toinetoine1.unitest.UniTest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by Toinetoine1 on 22/04/2020.
 */

public class InventoryCloseListener implements Listener {

    private UniTest uniTest;

    public InventoryCloseListener(UniTest uniTest) {
        this.uniTest = uniTest;
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event){
        Inventory inventory = event.getInventory();

        Optional<UUID> optional = uniTest.getInventories().entrySet().stream().filter(uuidInventoryEntry -> (uuidInventoryEntry.getValue().getTitle().equals(inventory.getTitle()))).map(Map.Entry::getKey).findAny();
        optional.ifPresent(uuid -> uniTest.getInventories().put(uuid, inventory));
    }
}
