package fr.toinetoine1.unitest.listeners;

import fr.toinetoine1.unitest.UniTest;
import fr.toinetoine1.unitest.utils.NBTChanger;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by Toinetoine1 on 22/04/2020.
 */

public class InventoryClickListener implements Listener {

    private UniTest uniTest;

    public InventoryClickListener(UniTest uniTest) {
        this.uniTest = uniTest;
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Inventory inventory = event.getInventory();
        ItemStack currentItem = event.getCurrentItem();

        if (currentItem != null && currentItem.getType() == Material.GLASS_BOTTLE && NBTChanger.hasNBTTag(currentItem, "uuid")) {
            Optional<UUID> optional = uniTest.getInventories().entrySet().stream().filter(uuidInventoryEntry -> (uuidInventoryEntry.getValue().getTitle().equals(inventory.getTitle()))).map(Map.Entry::getKey).findAny();

            if (!optional.isPresent())
                return;

            switch (event.getClick()) {
                case RIGHT:
                    event.setCancelled(true);
                    uniTest.getInventories().put(optional.get(), inventory);
                    event.getWhoClicked().openInventory(uniTest.getInventories().get(UUID.fromString(NBTChanger.getNBTTag(currentItem, "uuid"))));
                    break;
            }
        }
    }

}
