package fr.toinetoine1.unitest.listeners;

import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Toinetoine1 on 21/04/2020.
 */

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        event.setJoinMessage(null);

        ComponentBuilder joinText = new ComponentBuilder("Bonjour ")
                .append(player.getName()).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(player.getLevel() + " " + player.getUniqueId().toString()).create()))
                .append(" bienvenue sur le serveur IceCube").reset()
        ;


        player.spigot().sendMessage(joinText.create());
    }

}
