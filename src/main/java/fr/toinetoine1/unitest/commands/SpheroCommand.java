package fr.toinetoine1.unitest.commands;

import fr.toinetoine1.unitest.UniTest;
import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Toinetoine1 on 21/04/2020.
 */

public class SpheroCommand implements CommandExecutor {

    private UniTest uniTest;

    public SpheroCommand(UniTest uniTest) {
        this.uniTest = uniTest;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) return true;

        Player player = (Player) sender;

        new BukkitRunnable() {
            float radius = 0.1f;

            float y = 0;
            float yCoef = .08f;

            int current = 0;
            int loopNumber = (int) Math.ceil(2 / yCoef);

            double angle = 0;
            double angleCoef = Math.PI/3;

            @Override
            public void run() {
                if (current == loopNumber)
                    cancel();

                displayParticule(player, angle, radius, y);

                if (current * 2 >= loopNumber) {
                    this.radius -= .06;
                } else
                    this.radius += .06;

                this.current++;
                this.angle += this.angleCoef;
                this.y += this.yCoef;
            }
        }.runTaskTimer(uniTest, 0, 3);

        return false;
    }

    private void displayParticule(Player player, double angle, float radius, float y){
        double x = Math.cos(angle) * radius;
        double z = Math.sin(angle) * radius;
        player.spawnParticle(Particle.REDSTONE, player.getLocation().clone().add(x, y, z), 0, 0.01, 0, 0);

        x = Math.cos(angle + Math.PI) * radius;
        z = Math.sin(angle + Math.PI) * radius;
        player.spawnParticle(Particle.REDSTONE, player.getLocation().clone().add(x, y, z), 0, 1, 1, 0);
    }

}
