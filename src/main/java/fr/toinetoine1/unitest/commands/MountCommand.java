package fr.toinetoine1.unitest.commands;

import fr.toinetoine1.unitest.UniTest;
import fr.toinetoine1.unitest.entity.CustomEnderman;
import net.minecraft.server.v1_10_R1.World;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Created by Toinetoine1 on 21/04/2020.
 */

public class MountCommand implements CommandExecutor {

    private UniTest uniTest;

    public MountCommand(UniTest uniTest) {
        this.uniTest = uniTest;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) return true;

        Player player = (Player) sender;
        Location location = player.getLocation();
        World world = ((CraftWorld) location.getWorld()).getHandle();

        Entity enderman = new CustomEnderman(world, location).getBukkitEntity();
        enderman.setPassenger(player);
        uniTest.getCustomEntity().add(enderman);

        return false;
    }
}
